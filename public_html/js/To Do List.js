$(function () {
    var APPLICATION_ID = "062EC757-FB13-6BA9-FF98-1B66BCF4B900",
            SECRET_KEY = "55328B51-797E-C728-FFEA-9AFC17D55600",
            VERSION = "v1";

     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
         
        var postsCollection = Backendless.Persistence.of(Posts).find();
        
        console.log(postsCollection);
        
        var wrapper = {
            posts: postsCollection.data
        };
        
        Handlebars.registerHelper('format', function (time) {
            return moment(time).format("dddd, MMMM Do YYYY");
        });
        
        var blogScript = $("#blogs-template").html();
        var blogTemplate = Handlebars.compile(blogScript);
        var blogHTML = blogTemplate(wrapper);
        
        $('.main-container').html(blogHTML);
        
        $(document).on('click', '.white-out-post', function () {
       var checkListScript = $("#check-done-template").html();
       var checkListTemplate = Handlebars.compile(checkListScript);
       $('.main-container').html(checkListTemplate);
       Materialize.toast('Task Complete');       
});
       $(document).on('click', '.white-in-post', function () {
       var uncheckListScript = $("#check-done-template").html();
       var uncheckListTemplate = Handlebars.compile(uncheckListScript);
       $('.main-container').html(uncheckListTemplate);
       Materialize.toast('Task Incomplete');
});


});
function Posts(args){
    args = args || {};
    this.title = args.title || "";  
    this.authorEmail = args.authorEmail || "";
}
$(document).on('click', '.deleteA',function (event){
   Backendless.Persistence.of(Posts).remove(event.target.attributes.data.nodeValue);
   Materialize.toast('Task has been deleted', 2000);   
});
